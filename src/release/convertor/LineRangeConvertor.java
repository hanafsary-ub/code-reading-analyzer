package release.convertor;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import release.data.Range;
import release.utility.ImageUtility;

public class LineRangeConvertor {

	private static final int whiteLineWidth = 50;
	private static final int whiteLineHeight = 22;
	
	private Range codeArea;
	private BufferedImage image;
	private List<Range> lineRanges = new ArrayList<>();	
	public static void main(String[] args){
		try {
			BufferedImage image = ImageIO.read(new File("col2input.png"));
			LineRangeConvertor convertor = new LineRangeConvertor(image);
			List<Range> ranges = convertor.getLineRanges();
			for(Range range : ranges){
				paintSolidRectangle(image, range, new Color(255, 0, 0));
			}
			ImageIO.write(image, "png", new File("col2output.png"));
			System.out.print("finished");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void paintSolidRectangle(BufferedImage image, Range range, Color color){
		int r = color.getRed();
		int g = color.getGreen();
		int b = color.getBlue();
		
		for(int x = range.getLeftX(); x <= range.getRightX() && x >= 0 && x < image.getWidth(); x++){
			image.setRGB(x, range.getTopY(), ImageUtility.rgb(r, g, b));
			image.setRGB(x, range.getBottomY(), ImageUtility.rgb(r, g, b));
		}
		for(int y = range.getTopY(); y <= range.getBottomY() && y >= 0 && y < image.getHeight(); y++){
			image.setRGB(range.getLeftX(), y, ImageUtility.rgb(r, g, b));
			image.setRGB(range.getRightX(), y, ImageUtility.rgb(r, g, b));
		}
	}
		
	public LineRangeConvertor(BufferedImage image){
		this.image = image;
		parseMultiColsImage();
	}
	
	public List<Range> getLineRanges(){
		return lineRanges;
	}
	
	public Range getCodeArea(){
		return codeArea;
	}
	
	private void parseMultiColsImage(){
		codeArea = findCodeArea();
		int yBegin = seekColYBegin();
		int xBegin = seekColXBegin(codeArea.getLeftX());
		int xEnd = seekColXEnd(xBegin, yBegin);
		while(xBegin != -1){
			List<Range> ranges = new ArrayList<>(findMinRects(xBegin, xEnd, yBegin));
			lineRanges.addAll(expandRanges(ranges));
			xBegin = seekColXBegin(xEnd + 1);
			xEnd = seekColXEnd(xBegin, yBegin);
		}
	}
	
	
	private int seekColXBegin(int xBegin){
		for(int x = xBegin; x <= codeArea.getRightX(); x++){
			for(int y = codeArea.getTopY(); y <= codeArea.getBottomY(); y++){
				if(!isWhitePixel(x, y)){//白でなかったらxを列の左x座標として返す
					return x;
				}
			}
		}
		// 列が見つからなかったら-1が返る
		return -1;
	}
	
	private int seekColYBegin(){
		for(int y = codeArea.getTopY(); y <= codeArea.getBottomY(); y++){
			for(int x = codeArea.getLeftX(); x <= codeArea.getRightX(); x++){
				if(!isWhiteBlock(x, y)){
					return y;
				}
			}
		}
		return -1;
	}
	
	private int seekColXEnd(int xBegin, int yBegin){
		if(xBegin == -1) return -1;
		for(int x = xBegin; x <= codeArea.getRightX(); x++){
			if(isWhiteBlock(x, codeArea.getTopY(), 30, codeArea.getHeight())){
				return x;
			}
		}
		return -1;
	}
	
	private List<Range> findMinRects(int xBegin, int xEnd, int yBegin){
		List<Range> minRects = new ArrayList<>();
		Range range;
		int y = yBegin;
		while((range = findMinRect(xBegin, xEnd, y)) != null){
			minRects.add(range);
			y = range.getBottomY() + 1;
		}
		return minRects;
	}
	
	private Range findMinRect(int xBegin, int xEnd, int yBegin){
		Range minRect = null;
		int topY = seekTopY(xBegin, yBegin, xEnd, codeArea.getBottomY());
		if(topY != -1){
			int bottomY = seekBottomY(xBegin, topY, xEnd, codeArea.getBottomY());
			minRect = Range.createBy4Points(xBegin, topY, xEnd, bottomY);
		}
		return minRect;
	}
	
	private List<Range> expandRanges(List<Range> ranges){
		List <Range> expanded = new ArrayList<>(); 
		for(int i = 1; i < ranges.size(); i++){
			Range prev = ranges.get(i-1);
			Range next = ranges.get(i);
			int margin = (next.getTopY() - prev.getBottomY()) / 2;
			int last = expanded.size() - 1;
			if(i == 1){
				expanded.add(prev.expand(margin));
				expanded.add(next.expandTopAndSide(margin, expanded.get(expanded.size() - 1).getBottomY()));
			}else if(i == ranges.size() -1){
				Range lastRange = expanded.get(last);
				expanded.set(last, lastRange.expandOnlyBottom(margin));
				expanded.add(next.expand(margin));
			}else{
				Range lastRange = expanded.get(last);
				expanded.set(last, lastRange.expandOnlyBottom(margin));
				expanded.add(next.expandTopAndSide(margin, expanded.get(expanded.size() - 1).getBottomY()));
			}
		}
		return expanded;
	}
	
	private Range findCodeArea(){
		int yBegin = seekYBegin();
		int yEnd = seekYEnd();
		int xBegin = seekXBegin();
		int xEnd = seekXEnd();
		return Range.createBy4Points(xBegin, yBegin, xEnd, yEnd);
	}
	
	private int seekYBegin(){
		int height = image.getHeight();
		int width = image.getWidth();
		int startY = -1;
		
		BEGIN_SEEK:
		for(int y = 0; y < height; y += whiteLineHeight){
			for(int x = 0; x < width; x += whiteLineWidth){
				if(isWhiteBlock(x, y)){
					startY = y;
					break BEGIN_SEEK;
				}
				
			}
		}
		return startY;
	}
	
	private int seekYEnd(){
		int yEnd = image.getHeight() - whiteLineHeight - 1;
		END_SEEK:
			for(int y = yEnd; y >= 0; y -= whiteLineHeight){
				for(int x = 0; x < image.getWidth(); x += whiteLineWidth){
					if(isWhiteBlock(x, y)){
						yEnd = y + whiteLineHeight;
						break END_SEEK;
					}
				}
			}
		return yEnd;
	}
	
	private int seekXBegin(){
		int xBegin = 0;
		BEGIN_SEEK:
			for(int x = 0; x < image.getWidth(); x += whiteLineWidth){
				for(int y = 0; y < image.getHeight(); y += whiteLineHeight){
					if(isWhiteBlock(x, y)){
						xBegin = x;
						break BEGIN_SEEK;
					}
				}
			}
		return xBegin;
	}
	
	private int seekXEnd(){
		int xEnd = image.getWidth() - whiteLineWidth - 1;
		END_SEEK:
			for(int x = xEnd; x >= 0; x -= whiteLineWidth){
				for(int y = 0; y < image.getHeight(); y += whiteLineHeight){
					if(isWhiteBlock(x, y)){
						xEnd = x + whiteLineWidth;
						break END_SEEK;
					}
				}
			}
		return xEnd;
	}
	
	private boolean isWhitePixel(int x, int y){
		int r = ImageUtility.r(image.getRGB(x, y));
		int g = ImageUtility.g(image.getRGB(x, y));
		int b = ImageUtility.b(image.getRGB(x, y));
		return r == 255 && g == 255 && b == 255;
	}
	
	private boolean isWhiteBlock(int x, int y){
		int whiteCount = 0;
		WHITE_BLOCK:
			for(int yi = y; yi < y + whiteLineHeight && yi < image.getHeight(); yi++){
				for(int xi = x; xi < x + whiteLineWidth && xi < image.getWidth(); xi++){
					if(isWhitePixel(xi, yi)){
						whiteCount++;
					}else{
						break WHITE_BLOCK;
					}
					
				}
			}
		return whiteCount == whiteLineWidth * whiteLineHeight;
	}
	private boolean isWhiteBlock(int x, int y, int width, int height){
		int whiteCount = 0;
		WHITE_BLOCK:
			for(int yi = y; yi < y + height && yi < image.getHeight(); yi++){
				for(int xi = x; xi < x + width && xi < image.getWidth(); xi++){
					if(isWhitePixel(xi, yi)){
						whiteCount++;
					}else{
						break WHITE_BLOCK;
					}
					
				}
			}
		return whiteCount == width * height;
	}
	
	private int seekTopY(int xBegin, int yBegin, int xEnd, int yEnd){
		int top = -1;
		TOP:
		for(int y = yBegin; y <= yEnd; y++){
			for(int x = xBegin; x <= xEnd; x++){
				if(!isWhitePixel(x, y)){//白でなかったらその行の上部確定
					top = y;
					break TOP;
				}
			}
		}
		return top;
	}
	
	private int seekBottomY(int xBegin, int yBegin, int xEnd, int yEnd){
		for(int y = yBegin; y <= yEnd; y++){
			boolean allWhite = true;
			for(int x = xBegin; x <= xEnd; x++){
				if(!isWhitePixel(x, y)){
					allWhite = false;
					break;
				}
			}
			if(allWhite){//区間内ですべて白のピクセル行があればその1つ前がその行の下部
				return y - 1;
			}
		}
		return -1;
	}
}
