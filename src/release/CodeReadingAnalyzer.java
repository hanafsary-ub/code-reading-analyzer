package release;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import release.data.Answer;
import release.data.ExtendedQuestionStructure;
import release.data.QuestionSummary;
import release.data.Screenshot;
import release.data.TobiiData;
import release.data.TransitionMatrix;
import release.data.TransitionMatrixGroup;
import release.exception.NotAllocatedLineException;
import release.io.AnswersReader;
import release.io.OutputFolder;
import release.io.QuestionStructureReader;
import release.io.ScreenshotReader;
import release.io.SimpleWriter;
import release.io.TobiiDataReader;
import release.setting.QuestionStructure;

// Mainクラス
public class CodeReadingAnalyzer {
	private static long timeStart;
	private static List<TobiiData> allocatedInputs = new ArrayList<>();
	
	static{
		timeStart = System.currentTimeMillis();
	}
	
	public static void main(String[] args) throws IOException{
		System.out.println("--入力ファイル読み込み中");
		List<QuestionStructure> structures = new QuestionStructureReader().getSettingObjects();
		System.out.println("question_structure.jsonを読み込みました。");
		List<Screenshot> screenshots = new ScreenshotReader().getScreenshots();
		System.out.println("input_file/screenshotsを読み込みました。");
		List<TobiiData> inputs = new TobiiDataReader().getTobiiDataList();
		System.out.println("input_file/tobii_outputsを読み込みました。");
	
		System.out.println("--出力フォルダ構築, ファイル書き込み");
		// データ依存度のサマリーファイルの出力(q01.csvなど)
		writeSummaries(structures, screenshots, inputs);
		// 視線の頻度分布を出力する
		writeFrequencies(allocatedInputs);
		
		System.out.println("--出力完了");
		System.out.println(System.currentTimeMillis() - timeStart);
	}
	
	private static void writeSummaries(List<QuestionStructure> structures, List<Screenshot> screenshots, List<TobiiData> inputs){
		OutputFolder f = new OutputFolder();
		List<QuestionSummary> summaries = new ArrayList<>();
		List<ExtendedQuestionStructure> exStructures = ExtendedQuestionStructure.createExtendedStructures(structures, screenshots);
		for(ExtendedQuestionStructure ex : exStructures){
			System.out.println(ex.getName());
			ex.filterByQuestionName(inputs, "2"); // 例えばq1-1とq1-2のうちq1-2のみを扱う
			ex.allocateLineRanges();              // 行判定の座標範囲を設定する
			allocatedInputs.addAll(ex.getFilteredInputs());
			try {
				QuestionSummary qs = ex.createQuestionSummary();
				summaries.add(qs);
				f.writeIntermediateFiles(ex); // 行の座標範囲をスクリーンショット上にプロットしたものを出力
				f.write(qs);                  // 課題ごとにまとめたデータ依存度のサマリーを出力する
			} catch (NotAllocatedLineException e1) {
				e1.printStackTrace();
			}
		}
		f.writeSubjectSummaries(summaries); // 被験者ごとにまとめたデータ依存度のサマリーを出力する
	}
	
	private static void writeFrequencies(List<TobiiData> tobiiData){
		Map<String, Answer> reply = new AnswersReader("reply_answers.csv").getAnswers();
		Map<String, Answer> question = new AnswersReader("novices-2col.csv").getAnswers();
		List<TransitionMatrix> matrices = new ArrayList<>();
		for(TobiiData td : tobiiData){
			String replyAnswer = reply.get(td.getQuestionTitle()).getAnswers().get(td.getSubjectName().toLowerCase());
			String questionAnswer = question.get(td.getQuestionTitle()).getAnswers().get(td.getSubjectName().toLowerCase());
			TransitionMatrix matrix = new TransitionMatrix(td, replyAnswer, questionAnswer);
			matrices.add(matrix);
			String fileName = "";
			if(matrix.getQuestionAnswer().equals(AnswersReader.CORRECT)){
				fileName = matrix.getQuestionTitle() + "_正答_回答" + matrix.getReplyAnswer() + "_" + matrix.getSubjectName() + ".csv"; 
			}else{
				fileName = matrix.getQuestionTitle() + "_誤答_回答" + matrix.getReplyAnswer() + "_" + matrix.getSubjectName() + ".csv"; 
			}
			SimpleWriter.write(SimpleWriter.OUTPUT_PATH + "_frequency/個人/", fileName, matrix.format());
		}
		List<TransitionMatrixGroup> replyGroups = TransitionMatrixGroup.create(matrices, TransitionMatrixGroup.ReplyAnswer);
		for(TransitionMatrixGroup replyMatrix : replyGroups){
			SimpleWriter.write(SimpleWriter.OUTPUT_PATH + "_frequency/回答別/", replyMatrix.getGroupName() + ".csv", replyMatrix.format());
		}
		List<TransitionMatrixGroup> questionGroups = TransitionMatrixGroup.create(matrices, TransitionMatrixGroup.QuestionAnswer);
		for(TransitionMatrixGroup questionMatrix : questionGroups){
			SimpleWriter.write(SimpleWriter.OUTPUT_PATH + "_frequency/正誤別/", questionMatrix.getGroupName() + ".csv", questionMatrix.format());
		}
	}
}
