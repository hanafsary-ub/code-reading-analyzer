package release.data;

import java.util.List;
import java.util.Map;

/*
 * 複数のRecordを持つ
 * 各Recordのレコード名を除いた平均値を持つ
 */
public interface RecordGroup {
	public String getTitleName();
	public void setTitleName(String titleName);
	public List<Record> getRecords();
	public void setRecords(List<Record> records);
	public List<String> getColumnTitles();
	public void setColumnTitles(List<String> columnTitles);
	public Map<String, Double> getAverages();
	public void setAverages(Map<String, Double> averages);
	public String format();
}
