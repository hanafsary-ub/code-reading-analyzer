package release.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.io.AnswersReader;

public class SubjectSummary implements RecordGroup {
	private String titleName;
	private List<Record> questionRecords = new ArrayList<>();
	private List<String> columnTitles;
	private Map<String, Double> averages = new HashMap<>();
	private Map<String, Answer> questionAnswers;
	
	// 入力Tobiiデータは１人の被験者に限定されたもの
	public SubjectSummary(Map<String, Answer> questionAnswers){
		this.questionAnswers = questionAnswers;
		//calculateAverages();
	}
	
	// 各列の平均値を計算する
	private void calculateAverages(){
		boolean[] canSum = new boolean[questionRecords.size()];
		for(int i = 0; i < canSum.length; i++) canSum[i] = true;
		
		for(String title : columnTitles){
			double sum = 0;
			int count = 0;
			for(int i = 0; i < questionRecords.size(); i++){
				double value = questionRecords.get(i).getColumns().get(title);
				if(value != -1 && canSum[i]){
					sum += value;
					count++;
				}else{
					canSum[i] = false;
				}
			}
			averages.put(title, sum / count);
		}
	}
	@Override
	public String getTitleName() {
		return titleName;
	}

	@Override
	public void setTitleName(String titleName) {
		this.titleName = titleName.toLowerCase();
	}

	@Override
	public List<Record> getRecords() {
		return questionRecords;
	}

	@Override
	public void setRecords(List<Record> records) {
		questionRecords = records;
	}

	@Override
	public List<String> getColumnTitles() {
		return columnTitles;
	}

	@Override
	public void setColumnTitles(List<String> columnTitles) {
		this.columnTitles = columnTitles;
	}

	@Override
	public Map<String, Double> getAverages() {
		return averages;
	}

	@Override
	public void setAverages(Map<String, Double> averages) {
		this.averages = averages;
	}

	@Override
	public String format() {
		calculateAverages();
		StringBuilder sb = new StringBuilder("課題名, ").append(String.join(", ", this.columnTitles)).append("\n");
		// 各行：課題ごとの行
		for(Record rec : questionRecords){
			sb.append(rec.getRecordName()).append(", ");
			List<String> cols = new ArrayList<>();
			for(String title : this.columnTitles){
				cols.add(Double.toString(rec.getColumns().get(title)));
			}
			sb.append(String.join(", ", cols)).append(",");
		
			Map<String, String> answers = questionAnswers.get(rec.getRecordName().toLowerCase()).getAnswers();
			if(answers.get(titleName).equals(AnswersReader.CORRECT)){
				sb.append(AnswersReader.CORRECT);
			}else if(answers.get(titleName).equals(AnswersReader.INCORRECT)){
				sb.append(AnswersReader.INCORRECT);
			}else if(answers.get(titleName).equals(AnswersReader.INCORRECT_TIMEOUT)){
				sb.append(AnswersReader.INCORRECT_TIMEOUT);
			}else{
				sb.append("-");
			}
			sb.append("\n");
		}
		
		// 平均値の行
		sb.append("平均, ");
		List<String> cols = new ArrayList<>();
		for(String title : this.columnTitles){
			cols.add(Double.toString(this.getAverages().get(title)));
		}
		sb.append(String.join(", ", cols)).append("\n");
		
		return sb.toString();
	}

}
