package release.data;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.stream.Collectors;

public class Screenshot implements Cloneable{
	private String questionName;
	private BufferedImage image;
	
	public Screenshot(){
		
	}
	
	public Screenshot(String questionName, BufferedImage image){
		this.questionName = questionName;
		this.image = image;
	}
	
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	
	@Override
	public Screenshot clone(){
		Screenshot ss = null;
		try {
			ss = (Screenshot)super.clone();
			ss.image = copy(image);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return ss;
	}
	
	// https://stackoverflow.com/questions/3514158/how-do-you-clone-a-bufferedimage
	private BufferedImage copy(BufferedImage image){
		BufferedImage clone = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
	    Graphics2D g2d = clone.createGraphics();
	    g2d.drawImage(image, 0, 0, null);
	    g2d.dispose();
	    return clone;
	}
	
	public static List<Screenshot> clone(List<Screenshot> screenshots){
		return screenshots.stream().map(s -> s.clone()).collect(Collectors.toList());
	}
}
