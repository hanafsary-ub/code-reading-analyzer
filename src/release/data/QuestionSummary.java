package release.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import release.exception.NotAllocatedLineException;
import release.io.AnswersReader;

/**
 * 出力CSV用。被験者Subjectを問ごとにまとめてCSV出力する用にフォーマットされた文字列を生成する
 * @author hanafusaryo
 *
 */
public class QuestionSummary implements RecordGroup{
	
	private static final String DATA_DEPENDENCY = "データ依存度";
	private static final String READING_TIME = "読解時間(ms)";
	private static final String CORRECT_INCORRECT = "正誤";
	private String titleName;
	private List<Record> subjectRecords = new ArrayList<>();
	private List<String> columnTitles = new ArrayList<String>(){
		private static final long serialVersionUID = -7245953279042501280L;
		{
			add(DATA_DEPENDENCY);
			add(READING_TIME);
			add(CORRECT_INCORRECT);
		}
	};
	private Map<String, Double> averages = new HashMap<>();
	private Map<String, String> answers;
	private double correctAverage;
	private double incorrectAverage;
	
	public QuestionSummary(ExtendedQuestionStructure structure, List<TobiiData> filteredInputs, Map<String, Answer> questionAnswers) throws NotAllocatedLineException{
		this.titleName = structure.getName();
		answers = questionAnswers.get(titleName).getAnswers();
		this.buildRecords(structure, filteredInputs);
		this.calculateAverages();
		calclateCorrectIncorrectAverage();
	}
	
	// 各列の平均値を計算する
	private void calculateAverages(){
		boolean[] canSum = new boolean[subjectRecords.size()];
		for(int i = 0; i < canSum.length; i++) canSum[i] = true;
		
		for(String title : columnTitles){
			double sum = 0;
			int count = 0;
			for(int i = 0; i < subjectRecords.size(); i++){
				double value = subjectRecords.get(i).getColumns().get(title);
				if(value != -1 && canSum[i]){
					sum += value;
					count++;
				}else{
					canSum[i] = false;
				}
			}
			averages.put(title, sum / count);
		}
	}
	
	private void calclateCorrectIncorrectAverage(){
		double correctSum = 0;
		double incorrectSum = 0;
		int correctCount = 0;
		int incorrectCount = 0;
		for(Record record : subjectRecords){
			double value = record.getColumns().get(DATA_DEPENDENCY);
			String recordName = record.getRecordName().toLowerCase();
			if(answers.get(recordName).equals(AnswersReader.CORRECT)){
				correctSum += value;
				correctCount++;
			}else if(answers.get(recordName).equals(AnswersReader.INCORRECT) ||
					answers.get(recordName).equals(AnswersReader.INCORRECT_TIMEOUT)){
				incorrectSum += value;
				incorrectCount++;
			}
			correctAverage = correctSum / correctCount;
			incorrectAverage = incorrectSum / incorrectCount;
		}
	}
	
	public void recalculateAverages(){
		this.calculateAverages();
	}
	
	/**
	 * 問題ごとに抽出された入力データ(filtered)と問題構造(structure)から、出力用の行レコード(subjectRecords)を構築する
	 * @param structure データ依存構造をもつ問題構造
	 * @param inputs {@link QuestionSummary#filter(List, List, String)}によって抽出された入力データ
	 * @throws NotAllocatedLineException 入力視線データの持つ視線座標に対応する行番号が設定されていない場合の例外
	 */
	private void buildRecords(ExtendedQuestionStructure structure, List<TobiiData> filtered)
			throws NotAllocatedLineException{
		for(TobiiData data : filtered){
			if(data.getQuestionSection().equals("1")) continue;
			Record rec = new SubjectRecord();
			rec.setRecordName(data.getSubjectName());
			List<GazeEvent> gazes = GazeEvent.clone(data.getGazeEvents());
			
			rec.setColumns(new HashMap<String, Double>(){
				private static final long serialVersionUID = 3524927494633719966L;
				{
					put(DATA_DEPENDENCY, rec.toValidTransitionRate(gazes, structure));
					put(READING_TIME, (double)data.getReadingTime());
					if(answers.get(data.getSubjectName().toLowerCase()).equals(AnswersReader.CORRECT)){
						put(CORRECT_INCORRECT, 1.0);
					}else{
						put(CORRECT_INCORRECT, 0.0);
					}
				}
			});
			this.subjectRecords.add(rec);
		}
	}
	
	@Override
	public String getTitleName() {
		return this.titleName;
	}

	@Override
	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	@Override
	public List<Record> getRecords() {
		return this.subjectRecords;
	}

	@Override
	public void setRecords(List<Record> records) {
		this.subjectRecords = records;
	}

	@Override
	public List<String> getColumnTitles() {
		return this.columnTitles;
	}

	@Override
	public void setColumnTitles(List<String> columnTitles) {
		this.columnTitles = columnTitles;
	}

	@Override
	public Map<String, Double> getAverages() {
		return this.averages;
	}

	@Override
	public void setAverages(Map<String, Double> averages) {
		this.averages = averages;
	}

	@Override
	public String format() {
		StringBuilder sb = new StringBuilder("被験者名, ").append(String.join(", ", this.columnTitles)).append("\n");
		
		// 各行：被験者ごとの行
		for(Record rec : this.subjectRecords){
			sb.append(rec.getRecordName()).append(", ");
			List<String> cols = new ArrayList<>();
			for(String title : this.columnTitles){
				cols.add(Double.toString(rec.getColumns().get(title)));
			}
			sb.append(String.join(", ", cols)).append(",");
			String recordName = rec.getRecordName().toLowerCase();
			if(answers.get(recordName).equals(AnswersReader.CORRECT)){
				sb.append(AnswersReader.CORRECT);
			}else if(answers.get(recordName).equals(AnswersReader.INCORRECT)){
				sb.append(AnswersReader.INCORRECT);
			}else if(answers.get(recordName).equals(AnswersReader.INCORRECT_TIMEOUT)){
				sb.append(AnswersReader.INCORRECT_TIMEOUT);
			}else{
				sb.append("-");
			}
			sb.append("\n");
		}
		
		// 平均値の行
		sb.append("平均, ");
		List<String> cols = new ArrayList<>();
		for(String title : this.columnTitles){
			cols.add(Double.toString(this.getAverages().get(title)));
		}
		sb.append(String.join(", ", cols)).append("\n");
		sb.append("正答平均,").append(correctAverage).append("\n");
		sb.append("誤答平均,").append(incorrectAverage).append("\n");
		
		return sb.toString();
	}

}
