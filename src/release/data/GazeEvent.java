package release.data;

import java.util.ArrayList;
import java.util.List;

public class GazeEvent implements Cloneable{
	private int fixationDuration;  // 現在座標における停留時間(ms)
	private int timestamp;         // 計測開始からの現在時刻(ms)
	private int x;                 // x座標
	private int y;                 // y座標
	private int line = -1;         // x,y座標に相当する行(1-n) どの行にも相当しない場合-1
	private boolean allocation = false; // 行範囲が設定されているかどうか
	
	public GazeEvent(){
	}
	
	public GazeEvent(int fixationDuration, int timeStamp, int x, int y){
		this.fixationDuration = fixationDuration;
		this.timestamp = timeStamp;
		this.x = x;
		this.y = y;
	}
	
	public void allocateLineRange(List<Range> ranges){
		setAllocation(true);
		for(int i = 0; i < ranges.size(); i++){
			if(ranges.get(i).isWithin(x, y)){
				setLine(i + 1);
				break;
			}
		}
	}
	public boolean isAllocated(){
		return allocation;
	}
	
	public void setAllocation(boolean allocation){
		this.allocation = allocation;
	}
	
	public void setFixationDuration(int fixationDuration){
		this.fixationDuration = fixationDuration;
	}
	
	public int getFixationDuration(){
		return this.fixationDuration;
	}
	
	public void setTimestamp(int timeStamp){
		this.timestamp = timeStamp;
	}
	
	public int getTimestamp(){
		return this.timestamp;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public int getX(){
		return this.x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public int getY(){
		return this.y;
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}
	
	public static List<GazeEvent> clone(List<GazeEvent> gazes){
		List<GazeEvent> clone = new ArrayList<>();
		for(GazeEvent e : gazes){
			try {
				clone.add((GazeEvent)e.clone());
			} catch (CloneNotSupportedException e1) {
				e1.printStackTrace();
			}
		}
		return clone;
	}
}
