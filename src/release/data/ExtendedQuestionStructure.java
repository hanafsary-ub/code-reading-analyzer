package release.data;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import release.convertor.LineRangeConvertor;
import release.exception.NotAllocatedLineException;
import release.io.AnswersReader;
import release.setting.BaseQuestionStructure;
import release.setting.QuestionStructure;

public class ExtendedQuestionStructure extends BaseQuestionStructure{
	private List<Integer> validLines = new ArrayList<>(); // データ依存関係のある行 1-3,3-5なら1,3,5となる
	private List<Range> lineRanges = new ArrayList<>();
	private List<TobiiData> filteredInputs = new ArrayList<>();
	private BufferedImage codeImage;
	private Range codeArea;
	
	public static List<ExtendedQuestionStructure> createExtendedStructures(List<QuestionStructure> structures, List<Screenshot> screenshots){
		List<ExtendedQuestionStructure> ex = new ArrayList<>();
		for(QuestionStructure qs : structures){
			for(Screenshot ss : Screenshot.clone(screenshots)){
				if(qs.isSameName(ss.getQuestionName())){
					ex.add(new ExtendedQuestionStructure(ss, qs));
					break;
				}
			}
		}
		return ex;
	}
	
	public ExtendedQuestionStructure(Screenshot screenshot, QuestionStructure structure){
		name = structure.getName();
		oldName = structure.getOldName();
		validTransitions = structure.getValidTransitions();
		codeImage = screenshot.getImage();
		toValidLines();
		lineCount = structure.getLineCount();
		LineRangeConvertor convertor = new LineRangeConvertor(codeImage);
		lineRanges = convertor.getLineRanges();
		codeArea = convertor.getCodeArea();
	}
	
	public BufferedImage getCodeImage(){
		return codeImage;
	}
	
	public Range getCodeArea(){
		return codeArea;
	}
	
	public void filterByQuestionName(List<TobiiData> inputs, String section){
		inputs.parallelStream()
			.filter(data -> isSameName(data.getQuestionTitle()) && data.getQuestionSection().equals(section))
			.forEach(data -> filteredInputs.add(data.clone()));
	}
	
	public List<TobiiData> getFilteredInputs(){
		return filteredInputs;
	}
	public void allocateLineRanges(){
		for(TobiiData data : filteredInputs){
			List<GazeEvent> gazes = data.getGazeEvents();
			gazes.stream().forEach(g -> g.allocateLineRange(lineRanges));
		}
	}
	
	public QuestionSummary createQuestionSummary() throws NotAllocatedLineException{
		return new QuestionSummary(this, filteredInputs, new AnswersReader(AnswersReader.QuestionAnswerFileName).getAnswers());
	}
	
	public List<Integer> getValidLines(){
		return validLines;
	}
	
	public boolean isValidTransition(String prevLine, String nextLine){
		boolean valid = false;
		
		for(String vt : validTransitions){
			String[] line = vt.split("-");
			if((line[0].equals(prevLine) && line[1].equals(nextLine)) ||
			   (line[1].equals(prevLine) && line[0].equals(nextLine))){
				
				valid = true;
			}
		}
		return valid;
	}
	
	public List<Range> getLineRanges(){
		return lineRanges;
	}
	
	private void toValidLines(){
		for(String tr : validTransitions){
			String[] lines = tr.split("-");
			for(String line : lines){
				Integer i = Integer.parseInt(line);
				if(!validLines.contains(i)){
					validLines.add(i);
				}
			}
		}
	}
}
