package release.data;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class GazeError {
	private String subjectName;
	private Map<String, Double> errors = new TreeMap<>(); //キーがe1-1などのe<セクション番号>-<行番号>の文字列
	
	public GazeError(String subjectName){
		this.subjectName = subjectName;
	}
	
	public Map<String, Double> getErrors(){
		return errors;
	}
	
	public String getSubjectName() {
		return subjectName;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder(subjectName).append(" : ");
		for (Entry<String, Double> entry : errors.entrySet()) {
			sb.append("{").append(entry.getKey()).append(" : ").append(entry.getValue()).append("}, ");
		}
		return sb.toString();
	}
	
	public String toCSVFormat(){
		StringBuilder sb = new StringBuilder(subjectName).append(", ");
		for (Entry<String, Double> entry : errors.entrySet()) {
			sb.append(entry.getValue()).append(", ");
		}
		return sb.toString();
	}
}
