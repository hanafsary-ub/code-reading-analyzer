package release.data;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import release.exception.NotAllocatedLineException;

public class SubjectRecord implements Record {

	private String recordName;
	private Map<String, Double> columns;
	
	@Override
	public double toValidTransitionRate(List<GazeEvent> events, ExtendedQuestionStructure structure)
			throws NotAllocatedLineException{
		events = excludeUndifinedGaze(events);
		events = trimSameLineSequence(events);
		
		if(events.size() < 2) return -1;
		
		double sum = 0;
		String prev = this.tryGetGazeEventLine(events.get(0));
		for(int i = 1; i < events.size(); i++){
			String next = this.tryGetGazeEventLine(events.get(i));
			sum += (structure.isValidTransition(prev, next))? 1 : 0;
			prev = next;
		}
		return sum / (events.size() - 1); // 遷移の数は注視点-1	
	}
	
	private List<GazeEvent> excludeUndifinedGaze(List<GazeEvent> events){
		// ループ中の要素の削除を例外を発生することなくするにはこのような書き方にする必要がある
		Iterator<GazeEvent> iter = events.iterator();
		while(iter.hasNext()){
			if(iter.next().getLine() == -1){
				iter.remove();
			}
		}
		return events;
	}
	
	private List<GazeEvent> trimSameLineSequence(List<GazeEvent> events) throws NotAllocatedLineException{
		if(events.size() >= 2){
			GazeEvent prev = events.get(0);
			for(int i = 1; i < events.size(); i++){
				if(prev.getLine() == events.get(i).getLine()){
					// 停留時間を足し合わせる
					prev.setFixationDuration(prev.getFixationDuration() + events.get(i).getFixationDuration());
					events.remove(i--);
				}
				prev = events.get(i);
			}
		}
		return events;
	}
	
	private String tryGetGazeEventLine(GazeEvent event) throws NotAllocatedLineException{
		String line = "";
		if(event.isAllocated()){
			line = Integer.toString(event.getLine());
		}else{
			throw new NotAllocatedLineException();
		}
		return line;
	}
	
	@Override
	public String getRecordName() {
		return recordName;
	}

	@Override
	public void setRecordName(String recordName) {
		this.recordName = recordName;
	}

	@Override
	public Map<String, Double> getColumns() {
		return columns;
	}

	@Override
	public void setColumns(Map<String, Double> cols) {
		this.columns = cols;
	}
	

}
