package release.data;

import java.util.HashMap;
import java.util.Map;

public class Answer {
	protected String questionName;
	protected Map<String, String> answers = new HashMap<>();
	
	public Answer(String questionName){
		this.questionName = questionName;
	}
	
	public void addAnswer(String subjectName, String answer){
		answers.put(subjectName, answer);
	}
	
	public String getQuestionName(){
		return questionName;
	}
	
	public Map<String, String> getAnswers(){
		return answers;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder(questionName).append(" -> ");
		for(Map.Entry<String, String> e : answers.entrySet()) {
			sb.append("{").append(e.getKey()).append(": ").append(e.getValue()).append("}, ");
		}
		return sb.toString();
	}
}
