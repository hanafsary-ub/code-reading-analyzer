package release.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// QuestionSummaryを再編成してSubjectSummaryを構成する
public class SubjectSummaryConstructor {
	private Map<String, SubjectSummary> subjects = new HashMap<>();
	private List<QuestionSummary> questionSummaries;
	private Map<String, Answer> questionAnswers;
	public SubjectSummaryConstructor(List<QuestionSummary> questionSummaries
			, Map<String, Answer> questionAnswers){
		this.questionSummaries = questionSummaries; 
		this.questionAnswers = questionAnswers;
		arangeRecords();
	}
	private void arangeRecords(){
		for(QuestionSummary summary : questionSummaries){
			for(Record record : summary.getRecords()){
				// Mapに登録されていなかったら新たに生成して登録する
				if(subjects.get(record.getRecordName()) == null){
					SubjectSummary ss = new SubjectSummary(questionAnswers);
					ss.setTitleName(record.getRecordName());
					ss.setColumnTitles(summary.getColumnTitles());
					ss.getRecords().add(record);
					subjects.put(record.getRecordName(), ss);
				}else{
					subjects.get(record.getRecordName())
						.getRecords().add(record);
				}
				record.setRecordName(summary.getTitleName());
			}
		}
	}
	
	public Map<String, SubjectSummary> getSubjectSummaries(){
		return subjects;
	}
	//public static void main(String[] args){}
}
