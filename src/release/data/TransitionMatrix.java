package release.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import release.setting.TransitionMatrixFormat;

/***
 * 行推移の頻度もしくはマルコフ確率分布を扱うクラス
 * 個人もしくは正答誤答などで分けたグループとして扱うことも可能？別のクラスを用意するかもしれない
 * 各推移はマルコフ風に表すこともできれば数値が高い順に表示することもできる
 * 
 * @author hanafusaryo
 *
 */
public class TransitionMatrix {
	private String subjectName; //被験者名
	private String questionTitle; // q1-2の"q1"の部分
	private String questionSection; // q1-2の"2"の部分
	private String replyAnswer; // 回答
	private String questionAnswer; // 解答
	private Map<String, Double> matrix = new HashMap<>();
	
	/**
	 * 入力視線データから行列の構築をする。
	 * 頻度にするか、マルコフにするか、ソートに関しては設定ファイルで決めておく
	 */
	public TransitionMatrix(TobiiData tobiiData, String replyAnswer, String questionAnswer){
		subjectName = tobiiData.getSubjectName();
		questionTitle = tobiiData.getQuestionTitle();
		questionSection = tobiiData.getQuestionSection();
		this.replyAnswer = replyAnswer;
		this.questionAnswer = questionAnswer;
		toMatrix(tobiiData.getGazeEvents());
	}
	
	/**
	 * 注目行の時系列を累計もしくはマルコフモデルに変換する
	 * @param gazeEvents
	 */
	private void toMatrix(List<GazeEvent> gazeEvents){
		// 推移行列の初期化
		initMatrix();
		
		// 最初の-1でない行を探す
		int prev = -1;
		int firstIndex = 0;
		for(int fi = 0; fi < gazeEvents.size(); fi++){
			int line = gazeEvents.get(fi).getLine();
			if(line != -1){
				prev = line;
				firstIndex = fi + 1;
				break;
			}
		}
		
		if(prev == -1) return;
		
		for(int i = firstIndex; i < gazeEvents.size(); i++){
			int next = gazeEvents.get(i).getLine();
			if(next != -1 && prev != next){
				String key = prev + "→" + next;
				matrix.put(key, matrix.get(key) + 1);
				prev = next;
			}else{
				continue;
			}
		}
		if(TransitionMatrixFormat.IndivisualNormalization) normalize();
	}
	
	/**
	 * 最大頻度で正規化する
	 */
	private void normalize(){
		double max = 0;
		for(Entry<String, Double> entry : matrix.entrySet()){
			if(max < entry.getValue()){
				max = entry.getValue();
			}
		}
		if(max != 0){
			for(Entry<String, Double> entry : matrix.entrySet()){
				matrix.put(entry.getKey(), entry.getValue() / max);
			}
		}
	}
	
	/**
	 * 1->2から全てを0で初期化しておく
	 * 3行なら 1->2, 1->3, ..., 3->2で1->1や2->2,3->3は作らない
	 */
	private void initMatrix(){
		for(int i = 1; i <= TransitionMatrixFormat.LineCount; i++){
			for(int j = 1; j <= TransitionMatrixFormat.LineCount; j++){
				if(i != j){
					matrix.put(i + "→" + j, 0.0);
				}
			}
		}
	}
	
	/**
	 * CSV出力用の文字列に変換する
	 * この際にソートの設定を反映させてマップをソートする
	 * 出力の形もn x nか1 x n*nか変える
	 * @return
	 */
	public String format(){
		StringBuilder sb = new StringBuilder();
		if(TransitionMatrixFormat.IndivisualSort.equals("Markov")){
			// 列タイトル
			sb.append("i→j,");
			for(int j = 1; j <= TransitionMatrixFormat.LineCount; j++) sb.append("i→").append(j).append(",");
			sb.append("\n");
			
			// キー名でソート
			List<String> keylist = new ArrayList<String>(matrix.keySet());
			Collections.sort(keylist, new Comparator<String>() {
				  @Override
				  public int compare(String obj0, String obj1) {
					  return obj0.compareTo(obj1);
				  }
			});
			
			// 各行、8列溜まるごとに改行をして成形する。行と列が等しい位置にハイフンを挿入する
			int i = 0;
			int j = 0;
			sb.append(++i).append("→j,");
			for(String key : keylist){
				if(i-1 == j){
					sb.append("-,");
					j++;
				}
				sb.append(matrix.get(key));
				if(j < TransitionMatrixFormat.LineCount - 1){
					sb.append(",");
					j++;
				}else{
					sb.append("\n");
					j = 0;
					sb.append(++i).append("→j,");
				}
			}
			sb.append("-");
		}else{
			
		}
		return sb.toString();
	}
	
	public Map<String, Double> getMatrix(){
		return matrix;
	}
	
	public String getSubjectName(){
		return subjectName;
	}
	
	public String getQuestionTitle(){
		return questionTitle;
	}
	
	public String getQuestionSection(){
		return questionSection;
	}
	
	public String getReplyAnswer() {
		return replyAnswer;
	}
	
	public String getQuestionAnswer(){
		return questionAnswer;
	}
}
