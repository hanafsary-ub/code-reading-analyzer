package release.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import release.io.AnswersReader;
import release.setting.TransitionMatrixFormat;

/**
 * いくつかのtransition matrixをグループ分けして平均を求めるためのベースクラス
 * インターフェースにする？
 * @author hanafusaryo
 *
 */
public class TransitionMatrixGroup {
	public static final int ReplyAnswer = 0;
	public static final int QuestionAnswer = 1;
	protected String groupName;
	protected Map<String, Double> matrix = new HashMap<>();
	protected List<TransitionMatrix> filteredMatrices;
	
	public TransitionMatrixGroup(String groupName, List<TransitionMatrix> matrices){
		this.groupName = groupName;
		filteredMatrices = matrices;
		averageFilteredMatrices();
	}
	
	private void initMatrix(){
		for(int i = 1; i <= TransitionMatrixFormat.LineCount; i++){
			for(int j = 1; j <= TransitionMatrixFormat.LineCount; j++){
				if(i != j){
					matrix.put(i + "→" + j, 0.0);
				}
			}
		}
	}
	
	/**
	 * filteredMatricesの全ての推移ごとに頻度を加算し、filteredMatricesの大きさで割ることで平均頻度を算出する
	 */
	private void averageFilteredMatrices(){
		initMatrix();
		for(TransitionMatrix m : filteredMatrices){
			for(Map.Entry<String, Double> entry : m.getMatrix().entrySet()){
				matrix.put(entry.getKey(), matrix.get(entry.getKey()) + entry.getValue());
			}
		}
		for(Map.Entry<String, Double> entry : matrix.entrySet()){
			matrix.put(entry.getKey(), entry.getValue() / filteredMatrices.size());
		}
		if(TransitionMatrixFormat.GroupNormalization) normalize();
	}
	
	/**
	 * 最大頻度で正規化する
	 */
	private void normalize(){
		double max = 0;
		for(Entry<String, Double> entry : matrix.entrySet()){
			if(max < entry.getValue()){
				max = entry.getValue();
			}
		}
		if(max != 0){
			for(Entry<String, Double> entry : matrix.entrySet()){
				matrix.put(entry.getKey(), entry.getValue() / max);
			}
		}
	}
	
	public String format(){
		StringBuilder sb = new StringBuilder();
		if(TransitionMatrixFormat.GroupSort.equals("Desc")){
			sb.append("推移, 平均頻度\n");
			
			// MapをListにして降順にソートする
			List<Entry<String, Double>> entries = new ArrayList<Entry<String, Double>>(matrix.entrySet());
			Collections.sort(entries, new Comparator<Entry<String, Double>>() {
				public int compare(Entry<String, Double> obj1, Entry<String, Double> obj2)
	            {
	                return obj2.getValue().compareTo(obj1.getValue());
	            }
			});
			
			for(Entry<String, Double> entry : entries){
				sb.append(entry.getKey()).append(",").append(entry.getValue()).append("\n");
			}
		}else{
			// 行列形式の場合
		}
		return sb.toString();
	}
	
	public Map<String, Double> getMatrix(){
		return matrix;
	}
	
	public String getGroupName(){
		return groupName;
	}
	
	public static List<TransitionMatrixGroup> create(List<TransitionMatrix> matrices, int groupBy){
		// 回答ごとに分ける
		if(groupBy == ReplyAnswer){
			Map<String, List<TransitionMatrix>> answerMap = new HashMap<>();
			for(TransitionMatrix matrix : matrices){
				String key = matrix.getQuestionTitle() + "_" + matrix.getReplyAnswer();
				if(!answerMap.containsKey(key)){
					answerMap.put(key, new ArrayList<>());
				}
				answerMap.get(key).add(matrix);
			}
			List<TransitionMatrixGroup> groups = new ArrayList<>();
			for(Entry<String, List<TransitionMatrix>> entry : answerMap.entrySet()){
				groups.add(new TransitionMatrixGroup(entry.getKey(), entry.getValue()));
			}
			return groups;
		}else if(groupBy == QuestionAnswer){
			Map<String, List<TransitionMatrix>> answerMap = new HashMap<>();
			for(TransitionMatrix matrix : matrices){
				if(matrix.getQuestionAnswer().equals(AnswersReader.CORRECT)){
					String correctKey = matrix.getQuestionTitle() + "_正答";
					if(!answerMap.containsKey(correctKey)){
						answerMap.put(correctKey, new ArrayList<>());
					}
					answerMap.get(correctKey).add(matrix);
				}else{
					String incorrectKey = matrix.getQuestionTitle() + "_誤答";
					if(!answerMap.containsKey(incorrectKey)){
						answerMap.put(incorrectKey, new ArrayList<>());
					}
					answerMap.get(incorrectKey).add(matrix);
				}
			}
			List<TransitionMatrixGroup> groups = new ArrayList<>();
			for(Entry<String, List<TransitionMatrix>> entry : answerMap.entrySet()){
				groups.add(new TransitionMatrixGroup(entry.getKey(), entry.getValue()));
			}
			return groups;
		}else{
			return null;
		}
	}
	
}
