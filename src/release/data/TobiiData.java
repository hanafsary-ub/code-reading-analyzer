package release.data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import release.exception.NotAllocatedLineException;

public class TobiiData implements Cloneable{
	private String subjectName; //被験者名
	private String questionTitle; // q1-2の"q1"の部分
	private String questionSection; // q1-2の"2"の部分
	private int readingTime;
	private List<GazeEvent> gazeEvents = new ArrayList<>();
	
	public String getFullName(){
		return subjectName + "_" + questionTitle + "-" + questionSection;
	}
	
	public void allocateLineRanges(List<Range> ranges){
		gazeEvents.parallelStream().forEach(g -> g.allocateLineRange(ranges));
	}
	
	public String format() throws NotAllocatedLineException{
		StringBuilder sb = new StringBuilder("全体読解時間(ms),").append(readingTime).append("\n");
		sb.append("Timestamps(ms), 停留時間(ms), 行番号\n");
	
		for(GazeEvent ge : gazeEvents){
			if(ge.isAllocated()){
				sb.append(ge.getTimestamp()).append(", ").append(ge.getFixationDuration())
				  .append(",").append(ge.getLine()).append("\n");
			}else if(!ge.isAllocated()){
				throw new NotAllocatedLineException();
			}
		}
		return sb.toString();
	}
	
	public void trimGazeEvents(){
		gazeEvents = gazeEvents.stream().filter(g -> g.getLine() != -1).collect(Collectors.toList());
		List<GazeEvent> newGazes = new ArrayList<>();
		if(gazeEvents.size() >= 2){
			GazeEvent prev = gazeEvents.get(0);
			newGazes.add(prev);
			for(int i = 1; i < gazeEvents.size(); i++){
				if(prev.getLine() != gazeEvents.get(i).getLine()){
					newGazes.add(gazeEvents.get(i));
				}
				prev = gazeEvents.get(i);
			}
		}
		gazeEvents = newGazes;
	}
	
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	
	public String getQuestionTitle() {
		return questionTitle;
	}
	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}
	
	public String getQuestionSection() {
		return questionSection;
	}
	public void setQuestionSection(String questionSection) {
		this.questionSection = questionSection;
	}
	
	public int getReadingTime() {
		return readingTime;
	}
	public void setReadingTime(int readingTime) {
		this.readingTime = readingTime;
	}
	
	public List<GazeEvent> getGazeEvents() {
		return gazeEvents;
	}
	public void setGazeEvents(List<GazeEvent> gazeEvents) {
		this.gazeEvents = gazeEvents;
	}
	
	@Override
	public TobiiData clone(){
		TobiiData clone = null;
		try {
			clone = (TobiiData)super.clone();
			clone.gazeEvents = GazeEvent.clone(this.gazeEvents);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		
		return clone;
	}
	
	public static List<TobiiData> clone(List<TobiiData> tobiiList){
		List<TobiiData> copy = new ArrayList<>();
		
		for(TobiiData e : tobiiList){
			copy.add(e.clone());
		}
		
		return copy;
	}
	
}
