package release.data;

public class Range implements Cloneable{
	private int leftX = -1;
	private int topY = -1;
	private int rightX = -1;
	private int bottomY = -1;
	private int width = -1;
	private int height = -1;
	private static final int additionalWidth = 30;
	
	public Range(){
	}
	
	public Range(int leftX, int topY, int width, int height){
		this.leftX = leftX;
		this.topY = topY;
		rightX = leftX + width - 1;
		bottomY = topY + height - 1;
		this.width = width;
		this.height = height;
	}
	
	public static Range createBy4Points(int leftX, int topY, int rightX, int bottomY){
		return new Range(leftX,
				         topY,
				         rightX - leftX + 1,
				         bottomY - topY + 1); 
	}
	
	public Range expand(int margin){
		return createBy4Points(leftX - margin - additionalWidth, topY - margin, rightX + margin + additionalWidth, bottomY + margin);
	}
	
	public Range expandTopAndSide(int margin, int prevBottom){
		return createBy4Points(leftX - margin - additionalWidth, prevBottom + 1, rightX + margin + additionalWidth, bottomY);
	}
	
	public Range expandOnlyBottom(int margin){
		return createBy4Points(leftX, topY, rightX, bottomY + margin - 1);
	}
	public boolean isWithin(int x, int y){
		boolean within = false;
		if(x >= leftX && x < leftX + width &&
		   y >= topY && y < topY + height){
			
			within = true;
		}
		return within;
	}
	
	public void mightComputeLength(){
		if(leftX != -1 && topY != -1 && rightX != -1 && bottomY != -1){
			width = rightX - leftX + 1;
			height = bottomY - topY + 1;
		}
	}
	
	public void mightComputeRightBottom(){
		if(leftX != -1 && topY != -1 && width != -1 && height != -1){
			rightX = leftX + width - 1;
			bottomY = topY + height - 1;
		}
	}
	
	public int getLeftX() {
		return leftX;
	}
	public void setLeftX(int leftTopX) {
		this.leftX = leftTopX;
	}
	public int getTopY() {
		return topY;
	}
	public void setTopY(int leftTopY) {
		this.topY = leftTopY;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getRightX(){
		return rightX;
	}
	public int getBottomY(){
		return bottomY;
	}
	public void setRightX(int rightBottomX){
		this.rightX = rightBottomX;
	}
	public void setBottomY(int rightBottomY){
		this.bottomY = rightBottomY;
	}
	public String toString(){
		return new StringBuilder("[(").append(leftX).append(", ").append(topY).append(") to (")
				.append(rightX).append(", ").append(bottomY).append("), w:")
				.append(width).append(", h:").append(height).append("]").toString();
	}
}
