package release.data;

import java.util.List;
import java.util.Map;

import release.exception.NotAllocatedLineException;

// 出力ファイルの各データ部分
// QuestionSummaryが複数の被験者レコードを持つ
// また、全ての課題全体のQuestionSummaryがQuestionSummaryのレコードを持つ
// QuestionSummaryがレコードとなる場合、持っている各列の平均値を使う
public interface Record {
	public double toValidTransitionRate(List<GazeEvent> events, ExtendedQuestionStructure structure) throws NotAllocatedLineException;
	public String getRecordName();
	public void setRecordName(String recordName);
	public Map<String, Double> getColumns();// キー：列タイトル、値：列の値
	public void setColumns(Map<String, Double> cols);
}
