package release.setting;

public class TransitionMatrixFormat {
	public static String IndivisualSort = "Markov";
	public static String GroupSort = "Desc";
	public static String MatrixType = "Sum"; //or Markov
	public static boolean IndivisualNormalization = true; // 正規化するかどうか
	public static boolean GroupNormalization = true;
	public static int LineCount = 8;
}
