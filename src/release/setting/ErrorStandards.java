package release.setting;

import java.util.List;

public class ErrorStandards {
	private List<Integer> centerYCoordinates;

	public List<Integer> getCenterYCoordinates() {
		return centerYCoordinates;
	}

	public void setCenterYCoordinates(List<Integer> centerYCoordinates) {
		this.centerYCoordinates = centerYCoordinates;
	}
}
