package release.setting;

import java.util.List;

public abstract class BaseQuestionStructure {
	protected String name;
	protected String oldName;
	protected List<String> validTransitions;
	protected int lineCount;
	
	public boolean isSameName(String other){
		return name.equals(other) || oldName.equals(other);
	}

	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setOldName(String name){
		this.oldName = name;
	}
	public String getOldName(){
		return this.oldName;
	}
	public void setValidTransitions(List<String> transitions){
		this.validTransitions = transitions;
	}
	public List<String> getValidTransitions(){
		return this.validTransitions;
	}
	public int getLineCount() {
		return lineCount;
	}
	public void setLineCount(int lineLength) {
		this.lineCount = lineLength;
	}
}
