package release.exception;

public class TobiiColumnException extends Exception {

	private static final long serialVersionUID = 8389187577943012887L;
	//private static final String segmentDuration = "SegmentDuration";
	public static final String[] requiredColumns = {
		"SegmentDuration",
		"RecordingTimestamp",
		"GazeEventDuration",
		"FixationPointX (MCSpx)",
		"FixationPointY (MCSpx)"
	};

	public TobiiColumnException(String fileName, String[] lackingColumns){
		super(buildErrorMessage(fileName, lackingColumns));
	}
	
	private static String buildErrorMessage(String fileName, String[] lackingColumns){
		StringBuilder sb = new StringBuilder("入力ファイルに求められる列が含まれていません。:")
				.append(fileName).append("\n")
				.append("必要とされる要素：[")
				.append(String.join(",", requiredColumns))
				.append("]\n")
				.append("足りない要素：[")
				.append(String.join(",", lackingColumns))
				.append("]\n");
		return sb.toString();
	}
}
