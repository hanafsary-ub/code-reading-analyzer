package release.exception;

public class NotAllocatedLineException extends Exception {
	
	private static final long serialVersionUID = 5178324672072247346L;

	public NotAllocatedLineException(){
		super("視線データGazeEventに対応する行が設定されていません。");
	}
}
