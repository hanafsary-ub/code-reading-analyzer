package release.io;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import release.data.Screenshot;

public class ScreenshotReader {
	private static final String path = "./data/input_data/screenshots";
	private List<Screenshot> screenshots = new ArrayList<>();
	
	public ScreenshotReader(){
		try {
			this.initScreenshots(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public ScreenshotReader(String path) throws IOException{
		this.initScreenshots(path);
	}
	
	public List<Screenshot> getScreenshots(){
		return this.screenshots;
	}
	
	private void initScreenshots(String path) throws IOException{
		File[] files = new File(path).listFiles();
		for(File f : files){
			Pattern p = Pattern.compile("^q\\d{1,2}\\.png$");
			Matcher m = p.matcher(f.getName());
			if(m.find()){
				String name = this.toFileNameWithoutExtension(f.getName());
				BufferedImage image = ImageIO.read(f);
				screenshots.add(new Screenshot(name, image));
			}
		}
	}
	
	private String toFileNameWithoutExtension(String fileName){
		String noExtension = "";
		Pattern p = Pattern.compile("(.*)\\..*$");
		Matcher m = p.matcher(fileName);
		if(m.find()){
			noExtension = m.group(1);
		}
		return noExtension;
	}
}
