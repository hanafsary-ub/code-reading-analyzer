package release.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import release.data.Answer;

public class AnswersReader {
	private static final String path = "./data/input_data/answers/";
	public static final String QuestionAnswerFileName = "novices-2col.csv";
	public static final String ReplyAnswerFileName = "reply_answers.csv";
	public static final String CORRECT = "O";
	public static final String INCORRECT = "X";
	public static final String INCORRECT_TIMEOUT = "X(時間切れ)";
	
	private Map<String, Answer> answers = new HashMap<>();
	
	public AnswersReader(String fileName){
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path + fileName)));
			String[] colNames = br.readLine().split(",");
			for(int i = 1; i < colNames.length; i++){
				answers.put(colNames[i], new Answer(colNames[i]));
			}
			String line;
			while((line = br.readLine()) != null){
				String[] cols = line.split(",");
				String subjectName = cols[0];
				for(int i = 1; i < cols.length; i++){
					answers.get(colNames[i]).addAnswer(subjectName, cols[i]);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Map<String, Answer> getAnswers(){
		return answers;
	}
}
