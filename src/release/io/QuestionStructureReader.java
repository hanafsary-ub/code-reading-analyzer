package release.io;

import java.io.IOException;

import release.setting.QuestionStructure;

public class QuestionStructureReader extends SettingReader<QuestionStructure>{

	private static Class<QuestionStructure> clazz = QuestionStructure.class;
	
	static{
		filePath = "./settings/question_structure.json";
	}

	public QuestionStructureReader() throws IOException{
		super(clazz);
	}
	public QuestionStructureReader(Class<QuestionStructure> cls) throws IOException {
		super(cls);
	}
	public QuestionStructureReader(String jsonText, Class<QuestionStructure> cls){
		super(jsonText, cls);
	}

}
