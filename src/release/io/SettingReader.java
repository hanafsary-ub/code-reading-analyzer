package release.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.arnx.jsonic.JSON;
import net.arnx.jsonic.JSONEventType;
import net.arnx.jsonic.JSONReader;
import release.utility.FileReadingUtility;
/**
 * filePathは子クラスのstaticイニシャライザで初期化すること。
 * @author hanafusaryo
 *
 * @param <T>
 */
public abstract class SettingReader <T>{
	
	protected static String filePath;
	protected List<T> settingObjects = new ArrayList<>();
	protected Class<T> objectType;
	
	public SettingReader(Class<T> cls) throws IOException{
		this(FileReadingUtility.readAll(filePath), cls);
	}
	
	public SettingReader(String jsonText, Class<T> cls){
		this.readJsonObjects(jsonText, cls);
	}
	
	private void readJsonObjects(String jsonText, Class<T> cls){
		try {
			JSONReader jsonReader = new JSON().getReader(jsonText);
			
			JSONEventType type;
			while ((type = jsonReader.next()) != null) {
		        if (type == JSONEventType.START_OBJECT) {
		        	settingObjects.add(jsonReader.getValue(cls));
		        }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<T> getSettingObjects(){
		return this.settingObjects;
	}
	
	public static String getFilePath(){
		return filePath;
	}
}
