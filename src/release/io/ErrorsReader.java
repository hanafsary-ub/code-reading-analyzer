package release.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import release.data.GazeError;

public class ErrorsReader {
	private static final String path = "./data/input_data/errors/";
	private static final String yColumnName = "FixationPointY (MCSpx)";
	private int yColumnIndex = -1;
	private Map<String, GazeError> gazeErrors = new TreeMap<>(); // <被験者名, GazeError>
	private List<Integer> errorStandards;
	
	public ErrorsReader(){
		List<File> files = Arrays.stream(new File(path).listFiles()).filter(f -> f.getName().endsWith(".tsv")).collect(Collectors.toList());
		try {
			errorStandards = new ErrorStandardsReader().getSettingObjects().get(0).getCenterYCoordinates();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(File f : files){
			parseToGazeError(f);
		}
	}
	
	private void parseToGazeError(File file){
		if(isCorrectFormat(file.getName())){
			Pattern p = Pattern.compile("(.*)_(e\\d+-(\\d+))\\.tsv");
			Matcher m = p.matcher(file.getName());
			if(m.matches()){
				String subjectName = m.group(1);
				String errorName = m.group(2);
				String errorLine = m.group(3);
				// 誤差の基準となる座標を求める
				int errorStandard = errorStandards.get(Integer.parseInt(errorLine) -1);
				
				try {
					BufferedReader br = new BufferedReader(new FileReader(file));
					// 列タイトルから求めたい列が何番目の列にあるか探す
					findYColumnIndex(br.readLine());
					// 2行目以降からy座標の平均誤差を求める
					double average = calculateAverageError(br, errorStandard);
					// なければ作る
					if(gazeErrors.get(subjectName) == null){
						gazeErrors.put(subjectName, new GazeError(subjectName));
					}
					gazeErrors.get(subjectName).getErrors().put(errorName, average);
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void findYColumnIndex(String firstLine){
		String[] cols = firstLine.split("\t");
		if(yColumnIndex == -1){
			for(int i = 0; i < cols.length; i++){
				if(cols[i].equals(yColumnName)){
					yColumnIndex = i;
					break;
				}
			}
		}
	}
	
	// 2行目以降からy座標の平均誤差を求める
	private double calculateAverageError(BufferedReader br, int errorStandard) throws NumberFormatException, IOException{
		String line;
		double sum = 0;
		int count = 0;
		while((line = br.readLine()) != null){
			String y = line.split("\t")[yColumnIndex];
			if(!y.equals("null")){
				sum += Double.parseDouble(y);
				count++;
			}
		}
		return sum / count - errorStandard;
	}
	
	// "<被験者名>_e<セクション番号>-<行番号>.tsv"というファイル名になっているか
	private boolean isCorrectFormat(String fileName){
		Pattern p = Pattern.compile(".*_e\\d+-\\d+\\.tsv");
		Matcher m = p.matcher(fileName);
		return m.matches();
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for (Entry<String, GazeError> entry : gazeErrors.entrySet()) {
			sb.append(entry.getValue().toString()).append("\n");
		}
		return sb.toString();
	}
	
	public String toCSVFormat(){
		StringBuilder sb = new StringBuilder();
		String titles = null;
		for (Entry<String, GazeError> entry : gazeErrors.entrySet()) {
			if(titles == null){
				titles = String.join(", ", entry.getValue().getErrors().keySet());
				sb.append("被験者名, ").append(titles).append("\n");
			}
			sb.append(entry.getValue().toCSVFormat()).append("\n");
		}
		return sb.toString();
	
	}
	
	public void write(String path){
		File file = new File(path + "gaze_errors.csv");
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "SJIS"));
			bw.write(toCSVFormat());
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	public static void main(String[] args){
		ErrorsReader er = new ErrorsReader();
		er.write("./data/output_data/expand_r0.5_p0_j1/_errors/");
		System.out.println("誤差情報書き込み完了");
	}
}
