package release.io;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

import javax.imageio.ImageIO;

import release.data.ExtendedQuestionStructure;
import release.data.Range;
import release.data.Screenshot;
import release.data.TobiiData;
import release.exception.NotAllocatedLineException;
import release.utility.ImageUtility;

public class IntermediateFileWriter {

	private static final String imagePath = "_range_image/";
	private static final String linePath = "_line_outputs/";
	private static final int dottedLength = 5;
	private String path;
	
	public IntermediateFileWriter(String path){
		File imageFolder = new File(path + imagePath);
		if(!imageFolder.exists()) imageFolder.mkdirs();
		File lineFolder = new File(path + linePath);
		if(!lineFolder.exists()) lineFolder.mkdirs();
		this.path = path;
	}
	
	public void writeRangeImages(ExtendedQuestionStructure ex){
	
		BufferedImage image = ex.getCodeImage();
		Range codeArea = ex.getCodeArea();
		paintSolidRectangle(image, codeArea, new Color(0, 0, 255));
		List<Integer> validLines = ex.getValidLines();
		List<Range> ranges = ex.getLineRanges(); 
		for(int i = 1; i <= ex.getLineCount(); i++){
			boolean solid = false;
			for(Integer line : validLines){
				if(line == i){
					solid = true;
					break;
				}
			}
			if(solid){
				paintSolidRectangle(image, ranges.get(i-1), new Color(255, 0, 0));
			}else{
				paintDottedRectangle(image, ranges.get(i-1), new Color(0, 127, 127));
			}
		}
		try {
			ImageIO.write(image, "png", new File(path + imagePath + ex.getName() + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	}
	
	public void writeRangeImage(List<Screenshot> screenshots, List<ExtendedQuestionStructure> structures){
		for(Screenshot sc : screenshots){
			ExtendedQuestionStructure ex = structures.stream()
                    .filter(st -> sc.getQuestionName().equals(st.getName()))
                    .findFirst().get();
			BufferedImage image = sc.getImage();
			Range codeArea = ex.getCodeArea();
			paintSolidRectangle(image, codeArea, new Color(0, 0, 255));
			List<Integer> validLines = ex.getValidLines();
			List<Range> ranges = ex.getLineRanges();
			for(int i = 1; i <= ex.getLineCount(); i++){
				boolean solid = false;
				for(Integer line : validLines){
					if(line == i){
						solid = true;
						break;
					}
				}
				if(solid){
					paintSolidRectangle(image, ranges.get(i-1), new Color(255, 0, 0));
				}else{
					paintDottedRectangle(image, ranges.get(i-1), new Color(0, 127, 127));
				}
			}
			try {
				ImageIO.write(image, "png", new File(path + imagePath + sc.getQuestionName() + ".png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void paintSolidRectangle(BufferedImage image, Range range, Color color){
		int r = color.getRed();
		int g = color.getGreen();
		int b = color.getBlue();
		for(int x = range.getLeftX(); x <= range.getRightX() && x >= 0 && x < image.getWidth(); x++){
			image.setRGB(x, range.getTopY(), ImageUtility.rgb(r, g, b));
			image.setRGB(x, range.getBottomY(), ImageUtility.rgb(r, g, b));
		}
		for(int y = range.getTopY(); y <= range.getBottomY() && y >= 0 && y < image.getHeight(); y++){
			image.setRGB(range.getLeftX(), y, ImageUtility.rgb(r, g, b));
			image.setRGB(range.getRightX(), y, ImageUtility.rgb(r, g, b));
		}
	}
	
	private void paintDottedRectangle(BufferedImage image, Range range, Color color){
		int r = color.getRed();
		int g = color.getGreen();
		int b = color.getBlue();
		for(int x = range.getLeftX(); x <= range.getRightX(); x += dottedLength){
			if((x / dottedLength) % 2 == 0){
				for(int d = 0; d < dottedLength && x + d <= range.getRightX(); d++){
					if(x + d >= 0 && x + d < image.getWidth()){
						image.setRGB(x + d, range.getTopY(), ImageUtility.rgb(r, g, b));
						image.setRGB(x + d, range.getBottomY(), ImageUtility.rgb(r, g, b));
					}
				}
			}
		}
		for(int y = range.getTopY(); y <= range.getBottomY(); y += dottedLength){
			if((y / dottedLength) % 2 == 0){
				for(int d = 0; d < dottedLength && y + d <= range.getBottomY(); d++){
					if(y + d >= 0 && y + d < image.getHeight()){
						image.setRGB(range.getLeftX(), y + d, ImageUtility.rgb(r, g, b));
						image.setRGB(range.getRightX(), y + d, ImageUtility.rgb(r, g, b));
					}
				}
			}
		}
	}
	
	public void writeLineOutputs(List<TobiiData> inputs) throws NotAllocatedLineException{
		for(TobiiData data : inputs){
			File file = new File(path + linePath + data.getFullName() + ".csv");
			try {
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"SJIS"));
				bw.write(data.format());
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
}
