package release.io;

import java.io.IOException;

import release.setting.ErrorStandards;

public class ErrorStandardsReader extends SettingReader<ErrorStandards> {
	private static Class<ErrorStandards> clazz = ErrorStandards.class;
	
	static{
		filePath = "./settings/error_standards.json";
	}
	
	public ErrorStandardsReader() throws IOException{
		super(clazz);
	}
	
	public ErrorStandardsReader(Class<ErrorStandards> cls) throws IOException {
		super(cls);
	}
	
	public ErrorStandardsReader(String jsonText, Class<ErrorStandards> cls){
		super(jsonText, cls);
	}
	public static void main(String[] args) {
		try {
			ErrorStandards es = new ErrorStandardsReader().getSettingObjects().get(0);
			es.getCenterYCoordinates().forEach(c -> System.out.println(c));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
