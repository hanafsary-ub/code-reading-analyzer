package release.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class SimpleWriter {
	public static final String OUTPUT_PATH = "./data/output_data/";
	
	public static void write(String pathName, String fileName, String text){
		mkdirsIfNotExists(pathName);
		File file = new File(pathName + fileName);
		try{
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"SJIS"));
			bw.write(text);
			bw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private static void mkdirsIfNotExists(String pathName){
		File path = new File(pathName);
		if(!path.exists()){
			path.mkdirs();
		}
	}
}
