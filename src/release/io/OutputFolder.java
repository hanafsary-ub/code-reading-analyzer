package release.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import release.data.Answer;
import release.data.ExtendedQuestionStructure;
import release.data.QuestionSummary;
import release.data.SubjectSummary;
import release.data.SubjectSummaryConstructor;
import release.data.TobiiData;
import release.exception.NotAllocatedLineException;

public class OutputFolder {
	private static String folderPath = "./data/output_data/";
	private List<TobiiData> inputFiles;
	private List<ExtendedQuestionStructure> structures = new ArrayList<>();
	
	public void write(QuestionSummary group){
		String fileName = group.getTitleName() + ".csv";
		SimpleWriter.write(folderPath, fileName, group.format());
	}
	
	public void writeSubjectSummaries(List<QuestionSummary> questionSummaries){
		Map<String, Answer> questionAnswers = new AnswersReader(AnswersReader.QuestionAnswerFileName).getAnswers();
		SubjectSummaryConstructor ssc = new SubjectSummaryConstructor(questionSummaries, questionAnswers);
		Map<String, SubjectSummary> summaries = ssc.getSubjectSummaries();
		
		for(Map.Entry<String, SubjectSummary> entry : summaries.entrySet()) {
			String fileName = entry.getKey() + ".csv";
			SimpleWriter.write(folderPath, fileName, entry.getValue().format());
		}
	}
	
	public void writeIntermediateFiles(ExtendedQuestionStructure ex) throws NotAllocatedLineException{
		IntermediateFileWriter ifw = new IntermediateFileWriter(folderPath);
	
		ifw.writeLineOutputs(ex.getFilteredInputs());
		ifw.writeRangeImages(ex);
	}

	public void setExtendedStructures(List<ExtendedQuestionStructure> structures){
		this.structures = structures;
	}
	
	public List<ExtendedQuestionStructure> getExtendedStructures(){
		return structures;
	}
	
	public List<TobiiData> getInputFiles() {
		return inputFiles;
	}

	public void setInputFiles(List<TobiiData> inputFiles) {
		this.inputFiles = inputFiles;
	}
}
