package release.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import release.data.GazeEvent;
import release.data.TobiiData;
import release.exception.TobiiColumnException;

public class TobiiDataReader {

	private static final String path = "./data/input_data/tobii_outputs";
	public static final String SUBJECT_NAME = "Subject Name";
	public static final String QUESTION_TITLE = "Question Title";
	public static final String QUESTION_SECTION = "Question Section";
	
	private List<TobiiData> tobiiDataList = new ArrayList<>();
	private List<String> lackingColumns;
	private Map<String, Integer> columnOrder;
	
	public TobiiDataReader(){
		List<File>files = Arrays.stream(new File(path).listFiles()).filter(f -> f.getName().endsWith(".tsv")).collect(Collectors.toList());
		for(File f : files){
			tobiiDataList.add(this.toTobiiData(f));
		}
	}
	
	public TobiiData toTobiiData(File file){
		Map<String, String> fileInfo = extractFileInformationFrom(file.getName());
		TobiiData tobii = setUpGazeObject(file);
		tobii.setSubjectName(fileInfo.get(SUBJECT_NAME));
		tobii.setQuestionTitle(fileInfo.get(QUESTION_TITLE));
		tobii.setQuestionSection(fileInfo.get(QUESTION_SECTION));
		return tobii;
	}
	
	public List<TobiiData> getTobiiDataList(){
		return this.tobiiDataList;
	}
	
	private Map<String, String> extractFileInformationFrom(String fileName){
		Map<String, String> fileInfo = new HashMap<>();
		Pattern p = Pattern.compile("(.*)_(q\\d+)-([12])\\.tsv");
		Matcher m = p.matcher(fileName);
		
		if(m.matches()){
			fileInfo.put(SUBJECT_NAME, m.group(1));
			fileInfo.put(QUESTION_TITLE, m.group(2));
			fileInfo.put(QUESTION_SECTION, m.group(3));
		}
		
		return fileInfo;
	}
	
	private TobiiData setUpGazeObject(File file){
		TobiiData tobii = new TobiiData();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			
			this.scan(br.readLine().split("\\t"));
			// 最初の行(列タイトル)に必要な要素が含まれているか
			if(isContainingRequiredColumns(file.getName())){
				Map<String, Integer> order = this.getColumnOrder();
				int readingTime = -1;
				List<GazeEvent> gazeEvents = new ArrayList<>();
				String line;
				// 2行目以降の視線情報をGazeEventに
				while((line = br.readLine()) != null){
					String[] cols = line.split("\t");
					readingTime = Integer.parseInt(cols[order.get("SegmentDuration")]);
					gazeEvents.add(toGazeEvent(cols, order));
				}
				tobii.setReadingTime(readingTime);
				tobii.setGazeEvents(gazeEvents);
			}
			br.close();
		} catch (IOException | TobiiColumnException e) {
			e.printStackTrace();
		}
		return tobii;
	}
	
	private GazeEvent toGazeEvent(String[] cols, Map<String, Integer> order){
		int fixationDuration = Integer.parseInt(cols[order.get("GazeEventDuration")]);
		int timestamp = Integer.parseInt(cols[order.get("RecordingTimestamp")]);
		int x = Integer.parseInt(cols[order.get("FixationPointX (MCSpx)")]);
		int y = Integer.parseInt(cols[order.get("FixationPointY (MCSpx)")]);
		return new GazeEvent(fixationDuration, timestamp, x, y);
	}
	
	private boolean isContainingRequiredColumns(String fileName)
			throws TobiiColumnException
	{
		boolean allContainment = true;
		List<String> lackingColumns = this.getLackingColumns();
		
		// 不足要素が１つでもあれば例外を投げる
		if(!lackingColumns.isEmpty()){
			allContainment = false;
			String[] array = new String[lackingColumns.size()];
			throw new TobiiColumnException(fileName, lackingColumns.toArray(array));
		}
		return allContainment;
	}
	
	// 列タイトルの要素に必要なものが含まれているか、列の順番はどうか走査する
	private void scan(String[] columnTitles){
		lackingColumns = new ArrayList<String>();
		columnOrder = new HashMap<String, Integer>();
		
		for(String required : TobiiColumnException.requiredColumns){
			boolean containment = false;
			for(int i = 0; i < columnTitles.length; i++){
				// 正規表現だと括弧は構文の文字として認識される
				// 括弧も含めた部分一致比較がしたいので文字列としての括弧に変換して部分一致比較をしている。
				String regex = this.toRegex(required);
				if(columnTitles[i].matches(regex)){ // Tobiiの出力ファイルは１行目行頭にエディタでは確認できないゴミが混じる。そのため、完全一致でなく部分一致で比較をする
					containment = true;
					columnOrder.put(required, i);
					break;
				}
			}
			if(!containment){
				lackingColumns.add(required);
			}
		}
	}
	
	public List<String> getLackingColumns(){
		return this.lackingColumns;
	}
	
	public Map<String, Integer> getColumnOrder(){
		return this.columnOrder;
	}
	
	private String toRegex(String word){
		return ".*" + word.replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)") + ".*";
	}
}
