package release.utility;

/**
 * ARGB形式で表現された色情報から各色情報の抽出、各色情報からARGBA形式への変換を行うユーティリティクラスです。<br>
 * ARGB形式の色情報は以下のように8ビットごとに区切られ、全体で32ビットのビット列で表現されます。<br>
 * 00000000 00000000 00000000 00000000 (2進数。順にA R G B)<br><br>
 * この32ビットのビット列をcolorとした場合、各成分を取り出すには以下のようなビット演算をすることで取り出せます。<br>
 * <style>
 * table{border-collapse:collapse;}
 * th,td{border:solid 1px;padding:0.5em;}
 * </style>
 * <table>
 *   <tr>
 *     <td>Alpha</td><td>color &gt;&gt;&gt; 24</td><td>(右へ24ビット(3バイト)符号なしシフト)</td>
 *   </tr>
 *   <tr>
 *     <td>Red</td><td>color &gt;&gt; 16 &amp; 0xff</td><td>(右へ16ビット(2バイト)シフトして、AND演算で末尾8ビットのみを取り出す)</td>
 *   </tr>
 *   <tr>
 *     <td>Green</td><td>color &gt;&gt; 8 &amp; 0xff</td><td>(右へ8ビット(1バイト)シフトして、AND演算で末尾8ビットのみを取り出す)</td>
 *   </tr>
 *   <tr>
 *     <td>Blue</td><td>color &amp; 0xff</td><td>(AND演算で末尾8ビットのみを取り出す)</td>
 *   </tr>
 * </table>
 * <br>
 * このクラスではこれらのビット列を整数型で表現しています。<br>
 * ゆえにARGB各色成分から、ARGBの32ビットの色情報を取得するには以下のようなOR演算でビット操作する必要があります。<br>
 * A &lt;&lt; 24 | R &lt;&lt; 16 | G &lt;&lt; 8 | B
 * 
 * @author 花房亮
 *
 */
public class ImageUtility {
	
	/**
	 * ARGBで表された色情報から透明度を返します。
	 * 
	 * @param color 整数型ピクセルにパックされた各色8ビットARGB色成分
	 * @return RGBAで表された色情報うちの透明度
	 */
	public static int a(int color){
		return color >>> 24;
	}
	
	/**
	 * ARGBで表された色情報から赤色成分を返します。
	 * 
	 * @param color 整数型ピクセルにパックされた各色8ビットARGB色成分
	 * @return RGBAで表された色情報うちの赤色成分
	 */
	public static int r(int color){
		return (color >> 16) & 0xff;
	}
	
	/**
	 * ARGBで表された色情報から緑色成分を返します。
	 * 
	 * @param color 整数型ピクセルにパックされた各色8ビットARGB色成分
	 * @return RGBAで表された色情報うちの緑色成分
	 */
	public static int g(int color){
		return (color >> 8) & 0xff;
	}
	
	/**
	 * ARGBで表された色情報から青色成分を返します。
	 * 
	 * @param color 整数型ピクセルにパックされた各色8ビットARGB色成分
	 * @return RGBAで表された色情報うちの青色成分
	 */
	public static int b(int color){
		return color & 0xff;
	}
	
	/**
	 * 8ビットの整数型ピクセルにパックされた各色成分からARGB色成分を返します。<br>
	 * 透明度Aは不透明なものとして設定されます。
	 * 
	 * @param red   赤色成分
	 * @param green 緑色成分
	 * @param blue  青色成分
	 * @return 整数型ピクセルにパックされた各色8ビットARGB色成分
	 */
	public static int rgb(int red, int green, int blue){
		return 0xff000000 | red << 16 | green << 8 | blue;
	}
	
	/**
	 * 8ビットの整数型ピクセルにパックされた各色成分と透明度情報からARGB色成分を返します。
	 * 
	 * @param alpha 透明度
	 * @param red   赤色成分
	 * @param green 緑色成分
	 * @param blue  青色成分
	 * @return 整数型ピクセルにパックされた各色8ビットARGB色成分
	 */
	public static int argb(int alpha, int red, int green, int blue){
		return alpha << 24 | red << 16 | green << 8 | blue;
	}
}
